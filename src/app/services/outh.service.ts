import { Injectable,EventEmitter } from '@angular/core';
import { API_URL, GRANT_CLIENT_TOKEN } from '../models/const'
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BaseService, Session } from './base.service';
import { Storage } from '../models/storage';
import { User } from '../models/usuario.model';

@Injectable()
export class OuthService extends BaseService{

  authToken:string;
	headers:HttpHeaders;
	user:any;
	public loading: EventEmitter<boolean> = new EventEmitter();
	public errors:  EventEmitter<boolean> = new EventEmitter();
  

	constructor(
		protected http: HttpClient,
		private router: Router
	) {
		super(http);
		this.headers = new HttpHeaders({
			'Accept': 'application/json',
			'grant_type': 'client_credentials',
			'client_id': '1',
			'secret': GRANT_CLIENT_TOKEN
		});
		this.loading.emit(false);
		this.token_check()
	}


	check() : boolean {
		return Storage.check('session');
	}

	/**
	 * Finiliza la session actual
	 */
	signout() {
		Storage.clear();
		this.router.navigate(['/login']);
		return false;
	}
	/**
	* Iniciar sesión
	* @param {string} username Email del usuario.
	* @param {string} password Password del usuario.
	*/
	signin(username:string, pwd:string) {
		console.info('authenticando ...');
	  	if(Storage.getAll('session') === null) {
		  	this.loading.emit(true);
			this.post(API_URL+'/oauth/token', {
				'grant_type': 'password',
				'client_id': '2',
				'client_secret': GRANT_CLIENT_TOKEN,
				'username': username,
				'password': pwd
			})
			.subscribe( (res:any) => {
			  	if(res)
			  	this.loading.emit(false);
				this.sessionInit(res);
				Storage.setOne('userId', username);
				this.router.navigate([`/app/home`]);
			});
		} else {
			this.loading.emit(false);
			this.router.navigate([`/app/home/`]);
		}
	}


	updateUser() {
		this.get(API_URL+'/users/active')
			.subscribe( (data:any) => {
				Storage.setAll('user', data.user);
				Storage.setAll('roles', data.roles);
				Storage.setAll('company', data.user.company[0]);
		});
	}

	private token_check() : boolean {
		let session : Session = <Session>Storage.getAll('session');
		if( session ) {
			let countdown = (new Date().getTime() - session.start_session) / -3600000;
			if (countdown < 1) {

			};
		} 
		return false;
	} 

	registro(usuario: User, file: File){
		return new Promise( (resolve, reject) => {
		  let formData = new FormData();
		  let request = new XMLHttpRequest();
		  formData.append('_method', 'post');
		  Object.keys(usuario).forEach(function(b){
			if(usuario[b]){
				formData.append('user['+b+']', usuario[b]);
			}
		  });
		  if(file) formData.append('image', file, file.name);
		  request.onreadystatechange = () => {
			if(request.readyState === 4) {
			  if(request.status === 200){
				resolve(JSON.parse(request.response));
			  } else {
				reject(JSON.parse(request.response))
			  }
			}
		  };
		  request.open('post', `${API_URL}/api/user`, true);
		  request.setRequestHeader('Authorization', `Bearer ${ this.getToken() }`)
		  request.send(formData);
		});
	  }
	
	  /*changePhoto(idBuilding:string, file: File){
		return new Promise( (resolve, reject) => {
		  let formData = new FormData();
		  let request = new XMLHttpRequest();
		  formData.append('_method', 'put');
		  if(file) formData.append('image', file, file.name);
		  request.onreadystatechange = () => {
			if(request.readyState === 4) {
			  if(request.status === 201){
				resolve(JSON.parse(request.response));
			  } else {
				reject(JSON.parse(request.response))
			  }
			}
		  };
		  request.open('post', this.url.change_image+idBuilding, true);
		  request.send(formData);
		});
	  } */

	





}
