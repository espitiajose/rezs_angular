import { Injectable } from '@angular/core';
import { BaseService } from "./base.service";
import { HttpClient } from '@angular/common/http';
import { API_URL } from '../models/const';
import { Storage } from '../models/storage';
import { Recipe } from '../models/receta.model';
import { Ingredient } from '../models/ingrediente.model';
import { Comentario } from '../models/comentario.model';
import { Sugerencia } from '../models/sugerencia.model';

@Injectable()
export class AppService extends BaseService {

  constructor(protected http: HttpClient) {
    super(http);
   }

  recetasUsSeguidos(){
    return this.get(`${API_URL}/api/user/${Storage.getOne('userId')}/follow/recepies`)
  }

  userShow(userNick:string){
    return this.get(`${API_URL}/api/user/${userNick}`)
  }

  follow(userNick:string, userFollow){
    return this.post(`${API_URL}/api/user/${userNick}/follow/${userFollow}`, userFollow);
  }

  unFollow(userNick:string, userFollow){
    return this.remove(`${API_URL}/api/user/${userNick}/unfollow/${userFollow}`);
  }

  recetaShow(id:number){
    return this.get(`${API_URL}/api/recipes/${id}`)
  }

  recetas(url:string){
    return this.get(url)
  }

  deleteReceta(id:string){
    return this.remove(`${API_URL}/api/recipes/${id}`)
  }

  getCategorias(){
    return this.get(`${API_URL}/api/categoria`);
  }

  getCategoria(id:string){
    return this.get(`${API_URL}/api/categoria/${id}`);
  } 

  reseñar(comentario: Comentario){
    return this.post(`${API_URL}/api/comentarios/${comentario.id_receta}/${comentario.user_nick}`, {comentario})
  }
  sugerenciar(sugerencia: Sugerencia){
    return this.post(`${API_URL}/api/recetas/${sugerencia.id_receta}/user/${sugerencia.user_nick}/sugerencias`, {sugerencia})
  }

  searchName(search:string){
    return this.get(`${API_URL}/api/recipes/search/${search}`);
  }

  searchIngrediente(search:string){
    return this.get(`${API_URL}/api/ingredient/search/${search}`);
  }

  searchCategoria(idCategoria:number){
    return this.get(`${API_URL}/api/categorias/${idCategoria}/recetas`);
  }

  registrarReceta(receta:Recipe, ingredientes: Array<Ingredient>, file: File){
    return new Promise( (resolve, reject) => {
      let formData = new FormData();
      let request = new XMLHttpRequest();
      formData.append('_method', 'post');
      Object.keys(receta).forEach(function(b){
        if(receta[b]){
            formData.append('recipe['+b+']', receta[b]);
        }
      });
      for (let i = 0; i < ingredientes.length; i++) {
        const ingredients = ingredientes[i];
        Object.keys(ingredients).forEach(function(b){
          if(ingredients[b]){
              formData.append('ingredients['+i+']['+b+']', ingredients[b]);
          }
        });
      }
      if(file) formData.append('image', file, file.name);
      request.onreadystatechange = () => {
        if(request.readyState === 4) {
          if(request.status === 200){
            resolve(JSON.parse(request.response));
          } else {
            reject(JSON.parse(request.response))
          }
        }
      };
      request.open('post', `${API_URL}/api/recipes/new/`+Storage.getOne('userid'), true);
      request.setRequestHeader('Authorization', `Bearer ${ this.getToken() }`)
      request.send(formData);
    });
  }

}
