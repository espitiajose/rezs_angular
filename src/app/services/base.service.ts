import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from "../models/storage";
import { API_URL, GRANT_CLIENT_TOKEN } from "../models/const";
import { Observable } from 'rxjs';


export interface Session {
	token_type?:    string,
	expires_in?:    number,
	access_token?:  string,
	start_session?: number,
	refresh_token?: string
}

@Injectable()
export class BaseService {
  

  hds:HttpHeaders;
	httpOptions:{headers: HttpHeaders}
	constructor ( 
		protected http: HttpClient
	) {
		this.hds = new HttpHeaders({
			'Accept': 'application/json',
			'Authorization': `Bearer ${this.getToken()}`
		});
		this.httpOptions = { headers: this.hds}
	}

	sessionInit(session:Session) {
		let now = new Date().getTime()
		let day = 1000*60*60*24;
		Storage.setAll('session', {
			token_type: session.token_type,
			access_token: session.access_token,
			refresh_token: session.refresh_token,
			start_session: now+day,
			expires_in: session.expires_in
		});		
	}

	refesh_token() {
		let session : Session = this.getSession();
		let params = {
			'Accept': 'application/json',
			'client_id': '3',
			'client_secret': GRANT_CLIENT_TOKEN,
			'refresh_token': session.refresh_token
		};
		this.http.post(API_URL+'/oauth/token', params)
		.catch( (err) => {
			if(err.status === 401){
				console.log('Signin Error', err.error.message, 'error');
			} else {
				console.log('Server Error', "Couldn't establish connection with the server, please try again later.", 'error');
			}
			return Observable.throw( err );
		})
		.subscribe( (e) => {
			console.log(e);
		});
	}

	getSession() { return Storage.getAll('Session'); }
	getCompany() : any { 
		return Storage.getAll('company');
	 }
	getBuilding(): any { return Storage.getAll('Building'); }
	getUser() { 
		if(Storage.check('User')){
			return Storage.getAll('User');
		} else {
			return {id: '' };
		}
  }
  
	getToken() {
		let session:any = Storage.getAll('session');
		if(session)
			return session.access_token;
		else 
			return false;
	}

	signinCheck() { return Storage.getAll('session') !== null; }


	get( url: string, ) : Observable<Object> {
		return this.http.get(url, this.httpOptions);
	}

	post(url: string, body: any) {
		return this.http.post(url, body, this.httpOptions);
	}

	patch(url: string, body: any) {
		return this.http.patch(url, body, this.httpOptions);
	}

	put(url: string, body: any) {
		return this.http.put(url, body, this.httpOptions);
	}

	remove(url: string) {
		return this.http.delete(url, this.httpOptions);
	}

}
