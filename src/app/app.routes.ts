import {Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { NewRecetaComponent } from './pages/new-receta/new-receta.component';
import { ComentariosComponent } from './pages/comentarios/comentarios.component';
import { LoginComponent } from './login/login/login.component';
import { RegistroComponent } from './login/registro/registro.component';
import { PagesComponent } from './pages/pages.component';
import { NotpageComponent } from './pages/notpage/notpage.component';
import { BusquedaComponent } from './pages/busqueda/busqueda.component';
import { EditarPerfilComponent } from './pages/editar-perfil/editar-perfil.component';
import { IndexComponent } from "./index/index.component";
import { PerfilVisitComponent } from './pages/perfil-visit/perfil-visit.component';

const APPROUTES: Routes = [   
    { path: 'login', component: LoginComponent },
    { path: 'registro', component: RegistroComponent },    
    { path: 'app', component: PagesComponent, children: [
        { path: 'home', component: HomeComponent },
        { path: 'perfil/:userNick', component: PerfilComponent },
        { path: 'usuario/:userNick', component: PerfilVisitComponent },
        { path: 'editar', component: EditarPerfilComponent },
        { path: 'receta', component: NewRecetaComponent},
        { path: 'comentarios', component: ComentariosComponent },
        { path: 'search/:tipo/:search', component:  BusquedaComponent},
    ] },
    { path: '', component:  IndexComponent},
    { path: '**', component: NotpageComponent },
];

export const APP_ROUTES = RouterModule.forRoot(APPROUTES, { useHash: true });