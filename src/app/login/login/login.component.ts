import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OuthService } from '../../services/outh.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  forma: FormGroup;

  constructor(public outh: OuthService,
              private _r: Router) {
    if( this.outh.signinCheck() ) this._r.navigate(['/app/home']);
    this.forma = new FormGroup({
      userNick: new FormControl(null, Validators.required),
      contrasenia: new FormControl(null, Validators.required)
    });
   }

   login(){
     
     if(this.forma.valid){
       let usuario:string = this.forma.get('userNick').value;
       let password:string = this.forma.get('contrasenia').value;
       this.outh.signin(usuario, password); 
     }
   }


  ngOnInit() {

  }

}
