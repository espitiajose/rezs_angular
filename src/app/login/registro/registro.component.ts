import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OuthService } from '../../services/outh.service';
import { User } from '../../models/usuario.model';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
declare var $;


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  
  forma: FormGroup;
  @ViewChild('registroSwal') private registroSwal: SwalComponent;
  img: File;

  constructor(private _oS: OuthService) {
    this.forma = new FormGroup({
      nombre: new FormControl(null, Validators.required),
      apellido: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      userNick: new FormControl(null, Validators.required),
      img: new FormControl(null),
      contrasenia: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      contrasenia2: new FormControl(null, [Validators.required, Validators.minLength(6)])
    })
   }

   registrar(){
     let usuario = new User( 
       this.forma.get('userNick').value,
       this.forma.get('contrasenia').value,
       this.forma.get('nombre').value,
       this.forma.get('apellido').value,
       this.forma.get('email').value,
       '/assets/img/no_chef.png'
     );
     this._oS.registro(usuario, this.img).then((res:any)=>{
       if(res){
        this.registroSwal.show();
       }
     })
   }


  ngOnInit() {
    $("#fld-image").on('change', function () {
      if (typeof (FileReader) != "undefined") {
          var image_holder = $("#img-preiew");
          var reader = new FileReader();
          reader.onload = function (e:any) {
              var imgpath = 'url('+e.target.result+')';
              $(image_holder).css('background-image', imgpath);
              $(image_holder).css('background-repeat', 'no-repeat');
              $(image_holder).css('background-size', 'cover');
          }
          reader.readAsDataURL($(this)[0].files[0]);
      } else {
          alert("This browser does not support FileReader.");
      }
      });
  }

  changeImg(file:File){
    if(file)this.img = file;
    
  }

  ok(){
    window.history.back();
  }

}
