import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../../models/usuario.model';
import { Storage } from '../../models/storage';
import { AppService } from '../../services/app.service';
import { API_URL } from '../../models/const';

declare var $;

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  dataUser: any;
  usuario: any = {foto:'/assets/img/no_chef.png'};
  nickUsuario: string;
  recetas = [];
  followings = [];
  followers = [];
  photo:string = '';
  select: number = 1;
  nextPage: string = null;

  constructor(private _r:ActivatedRoute,
              public _aS: AppService) { 
    this.dataUser = Storage.getAll('dataUser');
    this.getUsuario(_r.snapshot.paramMap.get('userNick'));
    this.nickUsuario = _r.snapshot.paramMap.get('userNick');
  }

  ngOnInit() {
    if(!this.usuario.foto){
      this.usuario.foto = '/assets/img/no_chef.png'
    }
    this._aS.recetas(`${API_URL}/api/user/${this.nickUsuario}/recepies`).subscribe((res:any)=>{
      this.nextPage = res.next_page_url;
      for (let i = 0; i < res['data'].length; i++) {
        const receta = res['data'][i];
        this.recetas.push(receta);         
      }
    });
    $( (e) => {
      var $win = $(window);
      $win.scroll((e)=> {
        if ($win.height() + $win.scrollTop()  == $(document).height()) {
              if(this.nextPage){
                this._aS.recetas(this.nextPage).subscribe((res:any)=>{
                  this.nextPage = res.next_page_url;
                  for (let i = 0; i < res['data'].length; i++) {
                    const receta = res['data'][i];
                    this.recetas.push(receta);         
                  }
                });
              }
          }
      });
    });
  }

  buscarSeguidos(){
    for (let i = 0; i < this.usuario.following.length; i++) {
      const id = this.usuario.following[i].nick;
      this._aS.userShow(id).subscribe((res:any)=>{
        this.followings.push(res);
      });      
    }    
  }

  buscarSeguidores(){
    for (let i = 0; i < this.usuario.followers.length; i++) {
      const id = this.usuario.followers[i].nick;
      this._aS.userShow(id).subscribe((res:any)=>{
        this.followers.push(res);
      });      
    }  
  }

  changeSelect(n:number){
    this.select = n;
  }

  getUsuario(nick:string){
    this._aS.userShow(nick).subscribe((res:any)=>{
      this.dataUser = res;
      this.usuario = this.dataUser['Usuario:'];
      if(!this.usuario.foto){
        this.usuario.foto = '/assets/img/no_chef.png'
      }   
      
    this.buscarSeguidos();
    this.buscarSeguidores();   
    });
  }

} 
