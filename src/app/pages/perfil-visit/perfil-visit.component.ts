import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { API_URL } from '../../models/const';
import { Storage } from '../../models/storage';
import { AppService } from '../../services/app.service';
import { Observable } from 'rxjs';
declare var $;

@Component({
  selector: 'app-perfil-visit',
  templateUrl: './perfil-visit.component.html',
  styleUrls: ['./perfil-visit.component.css']
})
export class PerfilVisitComponent implements OnInit {

  dataUser: any = {NumRecetas:0,Seguidores:0, Siguiendo:0};
  usuario: any = {foto:'/assets/img/no_chef.png'};
  usuarioToken: any;
  recetas = [];
  followings = [];
  followers = [];
  photo:string = '';
  select: number = 1;
  nextPage: string = null;
  seg: boolean = false;

  constructor(private _r:ActivatedRoute,
              public _aS: AppService) { 
    this.getUsuario(_r.snapshot.paramMap.get('userNick'));
    let data = Storage.getAll('dataUser');
    this.usuarioToken = data['Usuario:'];
  }

  ngOnInit() { 
    $( (e) => {
      var $win = $(window);
      $win.scroll((e)=> {
        if ($win.height() + $win.scrollTop()  == $(document).height()) {
              if(this.nextPage){
                this._aS.recetas(this.nextPage).subscribe((res:any)=>{
                  this.nextPage = res.next_page_url;
                  for (let i = 0; i < res['data'].length; i++) {
                    const receta = res['data'][i];
                    this.recetas.push(receta);         
                  }
                });
              }
          }
      });
    });

    this._aS.recetas(`${API_URL}/api/user/${this._r.snapshot.paramMap.get('userNick')}/recepies`).subscribe((res:any)=>{
      this.nextPage = res.next_page_url;
      for (let i = 0; i < res['data'].length; i++) {
        const receta = res['data'][i];
        this.recetas.push(receta);         
      }
    });
  }

  buscarSeguidos(){
    this.followers = []
    for (let i = 0; i < this.usuario.following.length; i++) {
      const id = this.usuario.following[i].nick;
      this._aS.userShow(id).subscribe((res:any)=>{
        this.followings.push(res);
      });      
    } 
  }

  buscarSeguidores(){
    this.followers = []
    for (let i = 0; i < this.usuario.followers.length; i++) {
      const id = this.usuario.followers[i].nick;
      this._aS.userShow(id).subscribe((res:any)=>{
        this.followers.push(res);   
      });  
    } 
  }

  changeSelect(n:number){
    this.select = n;
  }

  getUsuario(nick:string){
    this._aS.userShow(nick).subscribe((res:any)=>{
      this.dataUser = res;
      this.usuario = this.dataUser['Usuario:'];
      if(!this.usuario.foto){
        this.usuario.foto = '/assets/img/no_chef.png'
      }      
      this.verficarSeg();
      this.actualizardatos();
    });
  }

  verficarSeg(){
    this.seg = false;
    for (let i = 0; i < this.usuario.followers.length; i++) {
      const nick = this.usuario.followers[i].nick;
      if(nick == this.usuarioToken.nick){
        this.seg = true;
      }
    }    
  }
  
  actualizardatos(){
    this._aS.userShow(this.usuario.nick).subscribe((res:any)=>{
      this.dataUser = res;
      this.usuario = this.dataUser['Usuario:'];
      if(!this.usuario.foto){
        this.usuario.foto = '/assets/img/no_chef.png'
      }
      this.buscarSeguidores();
      this.buscarSeguidos();
    });
  }

  follow(){
    this._aS.follow(this.usuarioToken.nick, this.usuario.nick).subscribe((res:any)=>{
      console.log(res)
    })
    setTimeout(() => {
      this.seg = true;
      this.actualizardatos();
    }, 1000);
  }

  unfollow(){
    this._aS.unFollow(this.usuarioToken.nick, this.usuario.nick)    
    .subscribe((res:any)=>{
      console.log(res)
    })    
    setTimeout(() => {
      this.seg = false;
      this.actualizardatos();
    }, 1000);
    
  }

}
