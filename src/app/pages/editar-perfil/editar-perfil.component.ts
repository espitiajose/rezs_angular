import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../../models/usuario.model';
import { Storage } from '../../models/storage';
declare var $:any;

@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.component.html',
  styleUrls: ['./editar-perfil.component.css']
})
export class EditarPerfilComponent implements OnInit {

forma: FormGroup;
usuario: User;
img: File;

  constructor() {
    this.forma = new FormGroup({
      nombre: new FormControl(null, Validators.required),
      apellido: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      userNick: new FormControl(null, Validators.required),
      img: new FormControl(null),
      contraseniaAct: new FormControl(null, Validators.required),
      contrasenia: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      contrasenia2: new FormControl(null, [Validators.required, Validators.minLength(6)])
    })
    this.usuario = Storage.getAll('dataUser')['Usuario:'];
   }

   editar(){
     
   }

   changeImg(file:File){
    if(file)this.img = file;
  }

  ngOnInit() {
    this.forma.get('nombre').setValue(this.usuario.nombre);
    this.forma.get('apellido').setValue(this.usuario.apellido);
    this.forma.get('email').setValue(this.usuario.email);
    this.forma.get('userNick').setValue(this.usuario.nick);
    $("#fld-image").on('change', function () {
      if (typeof (FileReader) != "undefined") {
          var image_holder = $("#img-preiew");
          var reader = new FileReader();
          reader.onload = function (e:any) {
              var imgpath = 'url('+e.target.result+')';
              $(image_holder).css('background-image', imgpath);
              $(image_holder).css('background-repeat', 'no-repeat');
              $(image_holder).css('background-size', 'cover');
          }
          reader.readAsDataURL($(this)[0].files[0]);
      } else {
          alert("This browser does not support FileReader.");
      }
      });
  }

}
