import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../../services/app.service';
import { User } from '../../models/usuario.model';
import { Storage } from '../../models/storage';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent implements OnInit {

  tipo: string = '';
  search: string = '';
  recetas = [];
  usuario: User;

  constructor(private _r: ActivatedRoute, private _aS: AppService) {
    this.tipo = _r.snapshot.paramMap.get('tipo');
    this.search = _r.snapshot.paramMap.get('search');
    let dataUser = Storage.getAll('dataUser');
    this.usuario = dataUser['Usuario:'];
   }

  ngOnInit() {
    switch (this.tipo) {
      case 'recetas':
      this._aS.searchName(this.search).subscribe((res:any)=>{
        for (let i = 0; i < res['data'].length; i++) {
          const receta = res['data'][i];
          this._aS.recetaShow(receta.id).subscribe((res:any)=>{
            this.recetas.push(res);
          })
        }
      })
        break;
        case 'ingrediente':
        this._aS.searchIngrediente(this.search).subscribe((res:any)=>{
          for (let i = 0; i < res.length; i++) {
            const element = res[i];
            this._aS.recetaShow(element.id_receta).subscribe((res:any)=>{
              this.recetas.push(res);
            })
            
          }
        })
        break;
        case 'categoria':
        this._aS.searchCategoria(+this.search).subscribe((res:any)=>{
          for (let i = 0; i < res.data.length; i++) {
            const element = res.data[i];
            this.recetas.push(element);            
          }
        })
        break;
    }
    
  }

}
