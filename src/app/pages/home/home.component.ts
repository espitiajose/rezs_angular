import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouterLinkActive, Router } from '@angular/router';
import { Recipe } from '../../models/receta.model';
import { User } from '../../models/usuario.model';
import { AppService } from '../../services/app.service';
import { API_URL } from '../../models/const';
import { Storage } from '../../models/storage';
declare var $;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  recetas : Array<any> = [];
  user: User;
  nextPage: string = null;
  dataUser: any;
  usuario: any;
  

  constructor(public _r: ActivatedRoute,
              public _aS: AppService) {
          this.dataUser = Storage.getAll('dataUser');
          this.usuario = this.dataUser['Usuario:'];
   }

  ngOnInit() {
    this._aS.recetasUsSeguidos().subscribe((res:any)=>{
      this.nextPage = res.next_page_url;
      for (let i = 0; i < res['data'].length; i++) {
        const receta = res['data'][i];
        this.recetas.push(receta);         
      }
    });

    $( (e) => { 
      var $win = $(window);
      $win.scroll((e)=> {
        if ($win.height() + $win.scrollTop()  == $(document).height()) {
              if(this.nextPage){
                this._aS.recetas(this.nextPage).subscribe((res:any)=>{
                  this.nextPage = res.next_page_url;
                  for (let i = 0; i < res['data'].length; i++) {
                    const receta = res['data'][i];
                    this.recetas.push(receta);         
                  }
                });
              }
          }
      });
    });
  }

  delete(idReceta:number){
    for (let i = 0; i < this.recetas.length; i++) {
      const element = this.recetas[i].receta;
      if(element.id == idReceta){
        this.recetas.splice(i, 1);
      }      
    }
  }

  

}
