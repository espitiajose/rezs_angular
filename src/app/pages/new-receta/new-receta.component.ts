import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Ingredient } from '../../models/ingrediente.model';
import { AppService } from '../../services/app.service';
import { Recipe } from '../../models/receta.model';
import { Categoria } from '../../models/categoria_receta.model';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
declare var $:any;

@Component({
  selector: 'app-new-receta',
  templateUrl: './new-receta.component.html',
  styleUrls: ['./new-receta.component.css']
})
export class NewRecetaComponent implements OnInit {

  forma: FormGroup;
  formaI: FormGroup;
  ingredientes: Array<Ingredient> = [];
  categorias: Array<Categoria> = [];
  err = '';
  img: File;
  @ViewChild('registroSwal') private registroSwal: SwalComponent;

  constructor(public _aS: AppService) { 
    this.forma = new FormGroup({
      nombre: new FormControl(null, Validators.required),
      categoria: new FormControl(null, Validators.required),
      preparacion: new FormControl(null, Validators.required),
      img: new FormControl(null),
    })
    this.formaI = new FormGroup({
      nombre: new FormControl(null, Validators.required),
      cantidad: new FormControl(null, [Validators.required, Validators.min(1)]),
      medicion: new FormControl(null, Validators.required)
    })
   }

   eliminarIngrediente(index:number){
     this.ingredientes.splice(index,1);
   }

   registrar(){
    if(this.forma.valid){
      if(this.ingredientes.length > 0){
        let receta = new Recipe(
          this.forma.get('nombre').value,
          this.forma.get('categoria').value,
          this.forma.get('preparacion').value
        )
        this._aS.registrarReceta(receta, this.ingredientes, this.img).then((res:any)=>{
          this.registroSwal.show();
        })
      }else{
        this.err = 'Debe ingresar al menos 1 ingrediente';
      }
    }
   }




   registrarI(){
     if(this.formaI.valid){
       let i = new Ingredient(
         this.formaI.get('nombre').value,
         this.formaI.get('cantidad').value,
         this.formaI.get('medicion').value
       );
       this.ingredientes.push(i);
       this.formaI.reset();
       $('#ingredienteModal').modal('hide')
     }
   }

  ngOnInit() { 
    $('[role="dialog"]').appendTo('body');
    this._aS.getCategorias().subscribe((res:Array<Categoria>)=>{
      this.categorias = res;
    });
    $("#fld-image").on('change', function () {
      if (typeof (FileReader) != "undefined") {
          var image_holder = $("#img-preiew");
          var reader = new FileReader();
          reader.onload = function (e:any) {
              var imgpath = 'url('+e.target.result+')';
              $(image_holder).css('background-image', imgpath);
              $(image_holder).css('background-repeat', 'no-repeat');
              $(image_holder).css('background-size', 'cover');
          }
          reader.readAsDataURL($(this)[0].files[0]);
      } else {
          alert("This browser does not support FileReader.");
      }
      });
  }

  changeImg(file:File){
    if(file)this.img = file;
  }
  ok(){
    window.history.back();
  }

}
