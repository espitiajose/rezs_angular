import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Ingredient } from '../../models/ingrediente.model';
import { Comentario } from '../../models/comentario.model';
import { AppService } from '../../services/app.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Sugerencia } from '../../models/sugerencia.model';
import { Storage } from '../../models/storage';

@Component({
  selector: 'app-receta',
  templateUrl: './receta.component.html',
  styleUrls: ['./receta.component.css']
})
export class RecetaComponent implements OnInit {



  @Input() res : any = {};
  @Input() usuario : any = {};
  @Output() idElim = new EventEmitter();
  usuarioToken : any;
  receta: any = {nombre:'', id_categoria:0, preparacion:''}
  ingredientes: Array<Ingredient> = []; 
  photo: any;
  comAct: boolean = false;
  surAct: boolean = false;
  comentarios: Array<Comentario> = [];
  sugerencias: Array<Sugerencia> = [];
  forma: FormGroup;

  constructor(private _aS: AppService) {
    this.usuarioToken = Storage.getAll('dataUser')['Usuario:'];
    this.forma = new FormGroup({
      comentario: new FormControl(null, Validators.required),
      calificacion: new FormControl(null)
    })
  }
  
  ngOnInit() {
    this.receta = this.res.receta;    
    if(this.receta.foto){
      this.photo = 'url('+ this.receta.foto +')';      
    }else{
      this.photo = 'url(/assets/img/no_receta.png)';      
    }
    if(!this.usuario.foto){
      this.usuario.foto = '/assets/img/no_chef_peq.png'
    }
    if(!this.usuarioToken.foto){
      this.usuarioToken.foto = '/assets/img/no_chef_peq.png'
    }  
    this.getData(); 
  }

  resena(){
    if(this.forma.valid){
      let comentario = new Comentario(
        this.receta.id,
        this.forma.get('comentario').value,
        this.forma.get('calificacion').value,
        this.receta.user_nick);
        this._aS.reseñar(comentario).subscribe((res:any)=>{
          if(res){
            this.comentarios.push(res);
            this.forma.reset();
          }
        })
    }
  }

  surgerencia(){
    if(this.forma.valid){
      let sugerencia = new Sugerencia(
        this.receta.id,
        this.forma.get('comentario').value, 
        this.receta.user_nick);
        this._aS.sugerenciar(sugerencia).subscribe((res:any)=>{
          if(res){
            this.sugerencias.push(res);
            this.forma.reset();
          }
        })
    }
  }

  coments(){
    this.comAct = !this.comAct;
    if(this.comAct){
      this.surAct = false;
    }
  }

  suger(){
    this.surAct = !this.surAct;
    if(this.surAct){
      this.comAct = false;
    }
  }

  delete(value){
    if(value){
    this._aS.deleteReceta(this.receta.id).subscribe((res:any)=>{
      this.idElim.emit(this.receta.id);
    })
  }
  }

  getData(){
    this._aS.recetaShow(this.receta.id).subscribe((res:any)=>{
      this.receta = res.receta;
      if(this.receta.foto){
        this.photo = 'url('+ this.receta.foto +')';      
      }else{
        this.photo = 'url(/assets/img/no_receta.png)';      
      }
      this.comentarios = this.receta.comentarios;
      this.sugerencias = this.receta.sugerencias; 
    })
  }

}
