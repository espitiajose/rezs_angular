import { Component, OnInit, Input } from '@angular/core';
import { Comentario } from '../../models/comentario.model';
import { AppService } from '../../services/app.service';
import { User } from '../../models/usuario.model';
import { Storage } from '../../models/storage';

@Component({
  selector: 'app-comentario',
  templateUrl: './comentario.component.html',
  styleUrls: ['./comentario.component.css']
})
export class ComentarioComponent implements OnInit {

  @Input() comentario: Comentario;
  usuario : any;

  constructor(private _aS:AppService) {
  }
  
  ngOnInit() {
    this._aS.userShow(this.comentario.user_nick).subscribe((res:any)=>{
      if(res){
        this.usuario = res['Usuario:']
        if(!this.usuario.foto){
          this.usuario.foto = '/assets/img/no_chef_peq.png' 
        }
      }
    })
  }

}
