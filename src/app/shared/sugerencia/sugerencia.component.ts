import { Component, OnInit, Input } from '@angular/core';
import { Sugerencia } from "../../models/sugerencia.model";
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-sugerencia',
  templateUrl: './sugerencia.component.html',
  styleUrls: ['./sugerencia.component.css']
})
export class SugerenciaComponent implements OnInit {

  @Input() sugerencia: Sugerencia;
  usuario:any;

  constructor(private _aS:AppService) { }

  ngOnInit() {
    this._aS.userShow(this.sugerencia.user_nick).subscribe((res:any)=>{
      if(res){
        this.usuario = res['Usuario:']
        if(!this.usuario.foto){
          this.usuario.foto = '/assets/img/no_chef.png' 
        }
      }
    })
  }

}
