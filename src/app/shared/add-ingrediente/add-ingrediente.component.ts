import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'add-ingrediente',
  templateUrl: './add-ingrediente.component.html',
  styleUrls: ['./add-ingrediente.component.css']
})
export class AddIngredienteComponent implements OnInit {

  forma: FormGroup;

  constructor() {
    this.forma = new FormGroup({
      nombre: new FormControl(null, Validators.required),
      cantidad: new FormControl(null, [Validators.required, Validators.min(1)]),
      medicion: new FormControl(null, Validators.required)
    })
   }

  ngOnInit() {
  }

}
