import { Component, OnInit } from '@angular/core';
import { User } from '../../models/usuario.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '../../models/storage';
import { AppService } from '../../services/app.service';
import { OuthService } from '../../services/outh.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  usuario: User = {nombre:'',apellido:'',nick:'',email:'', password:''};
  search: string = '';

  constructor(public _aS: AppService,
    public _o: OuthService,
    public _r: Router) {}

  ngOnInit() {    
    this._aS.userShow(Storage.getOne('userId')).subscribe((res:any)=>{
      this.usuario = res['Usuario:']
      Storage.setAll('dataUser', res);
      if(!this.usuario.foto){
        this.usuario.foto = '/assets/img/no_chef_peq.png'  
      }
    })
  }

  logOuth(){
    this._o.signout();
    this._r.navigate(['/login']);
  }

  busqueda(){
    this._r.navigate([`/app/search/recetas/${this.search}`]);
  }

}
