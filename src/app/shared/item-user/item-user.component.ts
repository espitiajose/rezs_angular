import { Component, OnInit, Input } from '@angular/core';
import { Storage } from '../../models/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'item-user',
  templateUrl: './item-user.component.html',
  styleUrls: ['./item-user.component.css']
})
export class ItemUserComponent implements OnInit {

  @Input() res: any;
  usuario: any;
  usuarioToken: any;
  

  constructor(private r: Router) {
    this.usuarioToken = Storage.getAll('dataUser')['Usuario:'];
   }

  ngOnInit() {
    this.usuario = this.res['Usuario:'];
    if(!this.usuario.foto){
      this.usuario.foto = '/assets/img/no_chef_peq.png';
    }
   
  }

  userclick(){
    if(this.usuario.nick == this.usuarioToken.nick){
      this.r.navigate(['/app/perfil/'+this.usuarioToken.nick])
    }else{
      this.r.navigate(['/app/usuario/'+this.usuario.nick])
    }
  }

}
