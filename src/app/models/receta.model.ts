
export class Recipe {
    constructor(
        public nombre:string,
        public id_categoria:number,
        public preparacion:string,
        public user_nick?: string,
        public calificacion?: number,
        public foto?: string,
        public id?:number,
        public created_at?:string,
    ){

    }
}