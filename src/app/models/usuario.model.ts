
export class User{
    constructor(
        public nick:string,
        public password:string,
        public nombre:string,
        public apellido:string,
        public email:string,
        public foto?:string,
        public rango?:string,
    ){}
}