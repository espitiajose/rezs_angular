export class Comentario{
    constructor(
        public id_receta: number,
        public comentario:string,
        public calificacion?:number,
        public user_nick?:string,
        public created_at?:string,
        public id?:number
    ){}
}