export class Storage {

	/**
	 * Remueve un valor en el almacenamiento local.
	 * @param {string} key key Nombre del valor a remover
	 */
	static remove ( key:string ) { localStorage.removeItem(key.toLowerCase()) }
	
	/**
	 * Obtiene Un valor tipo String del almacenamiento local.
	 * @param {string} key Nombre del valor a recuperar.
	 * @return {string} Retorna un string con el valor.
	 */
	static getOne ( key:string ) : string { return localStorage.getItem(key.toLowerCase()); }

	/**
	 * Obtiene un Objeto almacenado en el almacenamiento local.
	 * @param {string} key Nombre el objeto a recuperar.
	 * @return {Object} Retorna un string con el valor.
	 */
	static getAll ( key:string ) : Object { return JSON.parse( localStorage.getItem(key.toLowerCase()) ); }

	/**
	 * Almacena un valor String en el almacenamiento local.
	 * @param {string}	key		Nombre del valor.
	 * @param {string}	Value	Valor a almacenar.
	 */
	static setOne ( key:string, value:string ) { return localStorage.setItem( key.toLowerCase(), value ); }

	/**
	 * Almacena un Objeto en el almacenamiento local.
	 * @param {string}	key		Nombre del Objeto a almacenar.
	 * @param {any}		value	Valor para almacenar.
	 */
	static setAll ( key:string, value:any ) { return localStorage.setItem( key.toLowerCase(), JSON.stringify(value) ); }
	
	/**
	 * Remueve todos los datos del almacenamiento local.
	 * @returns {void} No retorna valor.
	 */
	static clear () : void { localStorage.clear(); }

	/**
	 * comprueba existencia de un valor en local storage
	 * @param {boolean} return Valor boolean si existe valor en el local storage.
	 */
	static check ( key:string ) : boolean { return localStorage.getItem( key.toLowerCase() ) !== null }
}
