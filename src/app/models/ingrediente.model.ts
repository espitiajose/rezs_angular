export class Ingredient{
    constructor(
        public nombre:string,
        public cantidad:number,
        public tipo_cantidad:string,
        public id?:number,
        public id_receta?: number,
    ){}
}