export class Sugerencia{
    constructor(
        public id_receta: number,
        public sugerencia:string,
        public user_nick:string,
        public id?:number,
        public created_at?:string
    ){}
}