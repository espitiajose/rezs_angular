import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';


// rutas
import { APP_ROUTES } from './app.routes';

import { PerfilComponent } from './pages/perfil/perfil.component';
import { NewRecetaComponent } from './pages/new-receta/new-receta.component';
import { RecetaComponent } from './shared/receta/receta.component';
import { ComentariosComponent } from './pages/comentarios/comentarios.component';
import { LoginComponent } from './login/login/login.component';
import { RegistroComponent } from './login/registro/registro.component';
import { PagesComponent } from './pages/pages.component';
import { NotpageComponent } from './pages/notpage/notpage.component';
import { BusquedaComponent } from './pages/busqueda/busqueda.component';
import { EditarPerfilComponent } from './pages/editar-perfil/editar-perfil.component';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { HomeComponent } from './pages/home/home.component';

//services
import { OuthService } from "./services/outh.service";
import { BaseService } from "./services/base.service";
import { ComentarioComponent } from './shared/comentario/comentario.component';
import { AddIngredienteComponent } from './shared/add-ingrediente/add-ingrediente.component';
import { AppService } from "./services/app.service";
import { SugerenciaComponent } from './shared/sugerencia/sugerencia.component';
import { IndexComponent } from './index/index.component';
import { ItemUserComponent } from './shared/item-user/item-user.component';
import { PerfilVisitComponent } from './pages/perfil-visit/perfil-visit.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    PerfilComponent,
    NewRecetaComponent,
    RecetaComponent,
    ComentariosComponent,
    LoginComponent,
    RegistroComponent,
    PagesComponent,
    NotpageComponent,
    BusquedaComponent,
    EditarPerfilComponent,
    ComentarioComponent,
    AddIngredienteComponent,
    SugerenciaComponent,
    IndexComponent,
    ItemUserComponent,
    PerfilVisitComponent,
  ],
  imports: [
    BrowserModule,
    APP_ROUTES,
    ReactiveFormsModule,
    FormsModule,
    TagInputModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SweetAlert2Module.forRoot()
  ],
  exports: [
    SugerenciaComponent, ItemUserComponent
  ],
  providers: [OuthService, BaseService, AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
